package com.example.managewholeday.fieldObserver

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import com.example.managewholeday.BR

class CreateNotePageActivityFieldObserver: BaseObservable() {
    @Bindable
    var title:String=""
        set(value) {
            field=value
            notifyPropertyChanged(BR.title)
        }

    @Bindable
    var description:String=""
        set(value) {
            field=value
            notifyPropertyChanged(BR.description)
        }

}