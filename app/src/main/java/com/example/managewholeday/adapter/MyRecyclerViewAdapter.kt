package com.example.managewholeday.adapter

import android.annotation.SuppressLint
import android.app.ActionBar
import android.app.Dialog
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.managewholeday.R
import com.example.managewholeday.databinding.LayoutCustomUpdateDialogBinding
import com.example.managewholeday.databinding.LayoutItemListBinding
import com.example.managewholeday.databinding.LayoutViewDataUserBinding
import com.example.managewholeday.roomDataBaseFiles.UserDataTable
import com.example.managewholeday.viewModel.CreateNoteActivityViewModel
import java.text.SimpleDateFormat
import java.util.Calendar

class MyRecyclerViewAdapter(
    private val userData : List<UserDataTable>,
    private val viewModel : CreateNoteActivityViewModel,
    private val context : Context,
) : RecyclerView.Adapter<MyRecyclerViewAdapter.MyViewHolder>() {

    private lateinit var dataForUpdate : UserDataTable

    inner class MyViewHolder(var binding : LayoutItemListBinding) :
        RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent : ViewGroup, viewType : Int) : MyViewHolder {
        val view : View? =
            LayoutInflater.from(parent.context).inflate(R.layout.layout_item_list, parent, false)
        val binding : LayoutItemListBinding = DataBindingUtil.bind(view!!)!!
        return MyViewHolder(binding)
    }

    override fun getItemCount() : Int {
        return userData.size
    }

    @SuppressLint("SuspiciousIndentation")
    override fun onBindViewHolder(holder : MyViewHolder, position : Int) {
        val user = userData[position]
        holder.binding.apply {
            this.tvTitle.text = user.title
            this.tvTitle.text = if (user.title.length > 15) "${
                this.tvTitle.text.substring(
                    0,
                    15
                )
            }..." else user.title
            this.tvDescription.text = user.description
            this.tvDescription.text = if (user.description.length > 20) "${
                this.tvDescription.text.substring( 
                    0,
                    20
                )
            }..." else user.description
            this.tvStatus.text = user.status

            this.imgBottonShowList.setOnClickListener {
                val layoutViewDataUserBinding =
                    LayoutViewDataUserBinding.inflate(LayoutInflater.from(context))
                val dialog = Dialog(context)
                dialog.setContentView(layoutViewDataUserBinding.root)
                dialog.setCancelable(false)
                dialog.show()
                val windowManager = WindowManager.LayoutParams()
                windowManager.width = ActionBar.LayoutParams.MATCH_PARENT
                windowManager.height = ActionBar.LayoutParams.WRAP_CONTENT
                dialog.window?.attributes = windowManager

                layoutViewDataUserBinding.showTitleViewUser.text=user.title
                layoutViewDataUserBinding.showDescriptionViewUser.text=user.description
                if (user.status == "Create"){
                    layoutViewDataUserBinding.showDateViewUser.text=user.createdDate
                }else{
                    layoutViewDataUserBinding.showDateViewUser.text=user.modifiedDate
                }
                layoutViewDataUserBinding.showStatusViewUser.text=user.status

                layoutViewDataUserBinding.btnOk.setOnClickListener {
                    dialog.dismiss()
            }
        }



            this.imgBottonDelete.setOnClickListener {
                viewModel.deleteSingleUser(user)
            }
            this.imgBottonUpdate.setOnClickListener {
                val layoutCustomUpdateDialogBinding =
                    LayoutCustomUpdateDialogBinding.inflate(LayoutInflater.from(context))
                val dialog = Dialog(context)
                dialog.setContentView(layoutCustomUpdateDialogBinding.root)
                dialog.setCancelable(false)
                dialog.show()
                val windowManager = WindowManager.LayoutParams()
                windowManager.width = ActionBar.LayoutParams.MATCH_PARENT
                windowManager.height = ActionBar.LayoutParams.WRAP_CONTENT
                dialog.window?.attributes = windowManager

                layoutCustomUpdateDialogBinding.elTitleInput.editText?.setText(user.title)
                layoutCustomUpdateDialogBinding.elDescriptionInput.editText?.setText(user.description)
                layoutCustomUpdateDialogBinding.btnUpdate.setOnClickListener {
                    val updatedTitle =
                        layoutCustomUpdateDialogBinding.elTitleInput.editText?.text.toString()
                    val updateDescription =
                        layoutCustomUpdateDialogBinding.elDescriptionInput.editText?.text.toString()

                    if (updatedTitle.isNotEmpty() && updateDescription.isNotEmpty()&&updatedTitle!=user.title || updateDescription!=user.description) {
                        val modifiedTimeData = setCurrentData()
                        val status = "Modified"
                        dataForUpdate = UserDataTable(
                            user.srNo,
                            updatedTitle,
                            updateDescription,
                            "",
                            modifiedTimeData,
                            status
                        )
                        viewModel.updateSingleUser(dataForUpdate)
                        dialog.dismiss()
                    } else {
                        Toast.makeText(context, "Please Enter Text ", Toast.LENGTH_SHORT).show()
                    }

                }
            }
        }
    }


}

private fun setCurrentData() : String {
    val date = Calendar.getInstance().time
    val formatter = SimpleDateFormat.getDateTimeInstance() //or use getDateInstance()
    return formatter.format(date)
}
