package com.example.managewholeday.repository

import androidx.lifecycle.LiveData
import com.example.managewholeday.roomDataBaseFiles.UserDataTable
import com.example.managewholeday.roomDataBaseFiles.UserDataTableDao
import kotlinx.coroutines.flow.Flow

class UserDataRepository (private val userDataTableDao : UserDataTableDao) {

    suspend fun insert(userDataTable : UserDataTable){
        userDataTableDao.insert(userDataTable)
    }

    val allUsers : LiveData<List<UserDataTable>> = userDataTableDao.getAllUsers()

    suspend fun deleteSingleUser(userDataTable : UserDataTable){
        userDataTableDao.deleteSingleUser(userDataTable)
    }

    suspend fun updateSingleUser(userDataTable : UserDataTable){
        userDataTableDao.updateSingleUser(userDataTable)
    }

    suspend fun deleteAllUsers(){
        userDataTableDao.deleteAll()
    }

     fun searchUser(searchResult:String){

    }

}
