package com.example.managewholeday.roomDataBaseFiles

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class UserDataTable(

    @PrimaryKey(autoGenerate = true)
    val srNo:Int=0,

    @ColumnInfo(name = "title")
    val title:String,

    @ColumnInfo(name = "description")
    val description:String,

    @ColumnInfo(name = "created_Date")
    val createdDate:String,

    @ColumnInfo(name = "modified_Date")
    val modifiedDate:String,

    @ColumnInfo(name = "status")
    val status:String
)
