package com.example.managewholeday.roomDataBaseFiles

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy.Companion.REPLACE
import androidx.room.Query
import androidx.room.Update


@Dao
interface UserDataTableDao {
    @Insert(onConflict = REPLACE)
    suspend fun insert(userDataTable : UserDataTable)

    @Query("Select * from userdatatable order by srNo ASC")
    fun getAllUsers(): LiveData<List<UserDataTable>>  // make live data to observe changes in list

    @Delete
   suspend fun deleteSingleUser(userDataTable : UserDataTable)

    @Update
   suspend fun updateSingleUser(userDataTable : UserDataTable)

    @Query("DELETE FROM UserDataTable")
    suspend fun deleteAll()


}