package com.example.managewholeday.viewModel

import android.annotation.SuppressLint
import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.asLiveData
import com.example.managewholeday.fieldObserver.CreateNotePageActivityFieldObserver
import com.example.managewholeday.repository.UserDataRepository
import com.example.managewholeday.roomDataBaseFiles.MyRoomDataBase
import com.example.managewholeday.roomDataBaseFiles.UserDataTable
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.util.Calendar

@SuppressLint("StaticFieldLeak")
class CreateNoteActivityViewModel(application: Application) : AndroidViewModel(application){
    private var title:String=""
    private var description=""
    private var timeAndDate:String=""
    private var status:String=""
    val createNotePageActivityFieldObserver= CreateNotePageActivityFieldObserver()

    private val userDataRepository :UserDataRepository
    private val allUsers : LiveData<List<UserDataTable>>
    init {
        val dao = MyRoomDataBase.getDatabase(application).getUserDao()
        userDataRepository= UserDataRepository(dao)
        allUsers = userDataRepository.allUsers
    }


    fun createNote():Int{
        var result =0
        title=createNotePageActivityFieldObserver.title
        description=createNotePageActivityFieldObserver.description
        timeAndDate=setCurrentData()
        status="Create"
        if (title.isNotEmpty() && description.isNotEmpty() && timeAndDate.isNotEmpty() && status.isNotEmpty()){
            val userDataTable=UserDataTable(0,title,description,timeAndDate,"",status)
            CoroutineScope(Dispatchers.IO).launch {
                userDataRepository.insert(userDataTable)

            }
            result=1
        }else{
            if (title.isEmpty()){
              result =2
            }
            if (description.isEmpty()){
                result= 3
            }

        }
      return result
    }

    fun getAllUsers():LiveData<List<UserDataTable>>{
        CoroutineScope(Dispatchers.Main).launch {
            userDataRepository.allUsers
        }
        return allUsers
    }

    private fun setCurrentData() : String {
        val date = Calendar.getInstance().time
        val formatter = SimpleDateFormat.getDateTimeInstance() //or use getDateInstance()
        return formatter.format(date)
    }

    fun deleteSingleUser(userDataTable : UserDataTable){
        CoroutineScope(Dispatchers.IO).launch {
            userDataRepository.deleteSingleUser(userDataTable)
        }
    }

    fun updateSingleUser(userDataTable : UserDataTable){
        CoroutineScope(Dispatchers.IO).launch {
            userDataRepository.updateSingleUser(userDataTable)
        }

    }

    fun deleteAllUsers(){
        CoroutineScope(Dispatchers.IO).launch {
            userDataRepository.deleteAllUsers()
        }
    }

     suspend fun searchUser(searchResult:String) {
         return userDataRepository.searchUser(searchResult)
    }

}
