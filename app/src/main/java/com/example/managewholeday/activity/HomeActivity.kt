package com.example.managewholeday.activity

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.managewholeday.R
import com.example.managewholeday.adapter.MyRecyclerViewAdapter
import com.example.managewholeday.databinding.ActivityHomeBinding
import com.example.managewholeday.viewModel.CreateNoteActivityViewModel
import com.google.android.material.navigation.NavigationBarView

class HomeActivity : AppCompatActivity(), View.OnClickListener,
    NavigationBarView.OnItemSelectedListener {
    private lateinit var binding : ActivityHomeBinding
    private lateinit var viewModel : CreateNoteActivityViewModel
    private lateinit var adapter : MyRecyclerViewAdapter
    override fun onCreate(savedInstanceState : Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_home)
        viewModel = ViewModelProvider(
            this,
            ViewModelProvider.AndroidViewModelFactory.getInstance(application)
        )[CreateNoteActivityViewModel::class.java]
        setViewOnRecycler()
        binding.fabBtnAdd.setOnClickListener(this)
    }

    private fun setViewOnRecycler() {
        binding.viewRecycler.layoutManager = LinearLayoutManager(this)
        viewModel.getAllUsers().observe(this) {
            adapter = MyRecyclerViewAdapter(it, viewModel, this@HomeActivity)
            binding.progressHorizontal.isVisible = false
            binding.viewRecycler.adapter = adapter
            binding.notifyChange()
        }
    }

    override fun onCreateOptionsMenu(menu : Menu?) : Boolean {
        val inflater : MenuInflater = menuInflater
        inflater.inflate(R.menu.menu_delete_option, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item : MenuItem) : Boolean {
        when (item.itemId) {

            R.id.menu_delete_All_item -> {
                viewModel.deleteAllUsers()
            }
        }

        return super.onOptionsItemSelected(item)
    }


    override fun onClick(v : View?) {
        startActivity(Intent(this, CreateNoteActivity::class.java))
    }

    override fun onNavigationItemSelected(item : MenuItem) : Boolean {
        when (item.itemId) {
            R.id.menu_home -> {
                Toast.makeText(this, "click on Home", Toast.LENGTH_SHORT).show()
            }

            R.id.menu_history -> {
                Toast.makeText(this, "click on History ", Toast.LENGTH_SHORT).show()
            }

            R.id.menu_profile -> {
                Toast.makeText(this, "click on Profile", Toast.LENGTH_SHORT).show()
            }

            R.id.menu_setting -> {
                Toast.makeText(this, "click on Setting", Toast.LENGTH_SHORT).show()
            }

        }
        return super.onOptionsItemSelected(item)
    }

}