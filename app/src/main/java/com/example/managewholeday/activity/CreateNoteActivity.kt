package com.example.managewholeday.activity

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.example.managewholeday.R
import com.example.managewholeday.databinding.ActivityCreateNoteBinding
import com.example.managewholeday.viewModel.CreateNoteActivityViewModel

class CreateNoteActivity : AppCompatActivity(), View.OnClickListener {
    private lateinit var binding: ActivityCreateNoteBinding
    private lateinit var viewModel: CreateNoteActivityViewModel
    @SuppressLint("AppCompatMethod")
    override fun onCreate(savedInstanceState : Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_create_note)
        viewModel = ViewModelProvider(
            this,
            ViewModelProvider.AndroidViewModelFactory.getInstance(application)
        )[CreateNoteActivityViewModel::class.java]
        binding.lifecycleOwner = this
        binding.myViewData = viewModel

        binding.btnCreate.setOnClickListener(this)
    }

    override fun onClick(v : View?) {
        when(viewModel.createNote()){
            1->{
                finish()
            }
            2->{
                binding.editTextTitle.error=" Title Can't be Empty "
            }
            3->{
                binding.editTextDescription.error=" Description Can't be Empty "
            }
        }
    }

}